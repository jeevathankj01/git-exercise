# Git Exercise

A dummy project to get familiar with Git Architecture.

Contributors Need to follow these steps
- clone the repository to your local machine
- create a new branch with name 'feature-1'
- checkout 'feature-1' branch
- add your name and email id in the Contributors list below
- commit the changes in your local repository
- push the updates to the remote repository
- raise a pull request to get it merged with the master branch


# Contributors

1. Prince M S - prince.mi3@iiitmk.ac.in
2. Sachin V S - sachin.mi20@iiitmk.ac.in
3. Shahal K P - shahal.mi20@iiitmk.ac.in
